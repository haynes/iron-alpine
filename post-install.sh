#!/usr/bin/env sh

# fail if a command fails
set -e
set -o pipefail

# remove apk package manager
find / -type f -iname '*apk*' -xdev -delete
rm -r /etc/apk
# keep the database of the installed packages so trivy can scan them
# rm -r /lib/apk
rm -r /usr/share/apk
rm -r /var/lib/apk
rm -r /var/cache/apk

# set rx to all directories, except data directory/
find "$APP_DIR" -type d -exec chmod 500 {} +

# set r to all files
find "$APP_DIR" -type f -exec chmod 400 {} +
chmod -R u=rwx "$DATA_DIR/"

# chown all app files
chown $APP_USER:$APP_USER -R $APP_DIR $DATA_DIR

# remove chown after use (links & binaries)
find / \( -type f -o -type l \) -iname 'chown' -xdev -delete

# Remove unnecessary commands
rm -fr /usr/bin/wget
rm -fr /usr/bin/unzip
rm -fr /usr/bin/bunzip2
rm -fr /usr/bin/bzip2

# remove networking related commands. Only use this if you don't have any outgoing traffic (untested)
#rm -fr /usr/bin/traceroute
#rm -fr /usr/bin/traceroute6
#rm -fr /usr/bin/nslookup

# finally remove this file
rm "$0"
